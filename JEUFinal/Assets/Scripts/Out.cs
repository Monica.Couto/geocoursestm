
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;


public class Out : MonoBehaviour
{

    public Button buttonMenu;
    public Button buttonQuitte;
    public TextMeshProUGUI Erreur2;
    public TextMeshProUGUI Ratio;
    //public int kakmsdk;
    
    void Start()
    {
        buttonMenu.onClick.AddListener(OnButtonMenuClicked);
        buttonQuitte.onClick.AddListener(OnButtonQuitteClicked);
        Erreur2.text= TEST.Erreur;
        Ratio.text = "Bravo vous avez fini le parcours avec un score de: "+TEST.Juste+ " / 20";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void OnButtonMenuClicked(){
       SceneManager.LoadScene("minigame");
    }
    private void OnButtonQuitteClicked(){
       Application.Quit();
       Debug.Log("out");
    }
    /*
    public void EnableButton(GameObject button)
    {
        button.SetActive(true);
        Background.SetActive(true);
    }

    // Function to disable a button
    public void DisableButton(GameObject button)
    {
        button.SetActive(false);
        Background.SetActive(false);
    }
    */
}
