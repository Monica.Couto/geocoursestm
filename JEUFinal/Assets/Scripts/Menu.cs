using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;


public class Menu : MonoBehaviour
{

    public Button buttonMenu;
    public Button buttonQuitte;
    //public int kakmsdk;
    
    void Start()
    {
        buttonMenu.onClick.AddListener(OnButtonMenuClicked);
        buttonQuitte.onClick.AddListener(OnButtonQuitteClicked);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void OnButtonMenuClicked(){
       SceneManager.LoadScene("minigame");
    }
    private void OnButtonQuitteClicked(){
       Application.Quit();
       Debug.Log("out");
    }
    /*
    public void EnableButton(GameObject button)
    {
        button.SetActive(true);
        Background.SetActive(true);
    }

    // Function to disable a button
    public void DisableButton(GameObject button)
    {
        button.SetActive(false);
        Background.SetActive(false);
    }
    */
}
