using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;


public class TEST : MonoBehaviour
{
    public GameObject Balle;
    public GameObject Background;
    public Button button1;
    public Button button2;
    public Button button3;
    public TextMeshProUGUI Text1;
    public TextMeshProUGUI Text2;
    public TextMeshProUGUI Text3;
    public TextMeshProUGUI Question;
    public TextMeshProUGUI TextAvancé;
    public static string Erreur = " ";
    private bool button1Clicked = false; // Track the state of button1
    private bool button2Clicked = false; // Track the state of button2
    private bool button3Clicked = false;
    private int NumNiveau = 0; 
    public static int Juste = 0;
    private int QuestionPosée =0;
    //private int retour =0;
    private QA[] array = new QA[100];
    private int[] ArrayNombre1 = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
    private int[] ArrayNombre2 = new int[] {20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39};
    private int[] ArrayNombre3 = new int[] {40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59};
    private int[] ArrayNombre4 = new int[] {60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79};
    private int[] ArrayNombre5 = new int[] {80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99};
    //private int[] ArrayNombreFaux = new int[20];
    private double Ratio=0.0;
    private int NumQest=0;
    private int NumQestCalcule=0;
    private int Question1=0;
    private int Question2=0;
    private int Question3=0;
    private int Question4=0;
    private int Question5=0;


    private double distance;
    private double distanceD;
    private double Delta;
    private float posRetour;
    
    // Start is called before the first frame update
    void Start()
    {
    
        initialise();
        NumQestCalcule= 0;
        Question.text = " ";
        TextAvancé.text= " ";
        Erreur = " ";
        distance = Balle.transform.position.z;
        Delta = 17;
        distanceD = distance-Delta;
        Juste = 0;
        //Ratio = 0.41;
        // Attach click event listener to button1
        button1.onClick.AddListener(OnButton1Clicked);
        button2.onClick.AddListener(OnButton2Clicked);
        button3.onClick.AddListener(OnButton3Clicked);
        DisableButton(button1.gameObject);
        DisableButton(button2.gameObject);
        DisableButton(button3.gameObject);
        Background.SetActive(false);

        Shuffle(ArrayNombre1);
        Shuffle(ArrayNombre2);
        Shuffle(ArrayNombre3);
        Shuffle(ArrayNombre4);
        Shuffle(ArrayNombre5);
        
    }

    
    // Update is called once per frame
    void Update()
    {   
        
        distance = Balle.transform.position.z;
        distanceD = distance-Delta;
        //Debug.Log("TEST");
        //Debug.Log("D"+Delta+"di"+ distanceD + "pos"+distance);
        // Activate button1 if distance is less than 3 and button is not clicked
        if (distanceD > 0 && !button1Clicked && !button2Clicked && !button3Clicked && QuestionPosée <20)
        {
            //Debug.Log(positionX());
            //Debug.Log("loopA"+loop);
            if (NumQestCalcule==0){
                CalculeNum();
                NumQestCalcule= 1;
            }
            

            Text1.text = array[NumQest].itemName1;
            Text2.text = array[NumQest].itemName2;
            Text3.text = array[NumQest].itemName3;
            Question.text= array[NumQest].questionItem;
            EnableButton(button1.gameObject);
            EnableButton(button2.gameObject);
            EnableButton(button3.gameObject);
        
            
        }else if (QuestionPosée>=20) {
            SceneManager.LoadScene("Outro");
            //Question.text= "Bravo vous avez fini le parcours avec un score de: "+Juste+ " / " + QuestionPosée + "\n"+Erreur;
        }
        
    }
    private void OnButton1Clicked(){
        //Debug.Log("B1");
        DisableButton(button1.gameObject);
        DisableButton(button2.gameObject);
        DisableButton(button3.gameObject);
        // Move Balle to a new position and increment NumNiveau
    
        if (Text1.text == array[NumQest].repJuste ){
            buttonPressedIf();
        }else{
            buttonPressedElse();
        }
        
        //Debug.Log("retour"+ retour);

        button1Clicked = false;
        buttonPressed();
    }

    private void OnButton2Clicked()
    {
        //Debug.Log("B2");
        DisableButton(button1.gameObject);
        DisableButton(button2.gameObject);
        DisableButton(button3.gameObject);

        if (Text2.text == array[NumQest].repJuste ){
           buttonPressedIf();
        }else{
            buttonPressedElse();
        }
        
        //Debug.Log("retour"+ retour);

        button2Clicked = false;
        buttonPressed();
    }

    private void OnButton3Clicked(){
        
        //Debug.Log("B3");
        DisableButton(button1.gameObject);
        DisableButton(button2.gameObject);
        DisableButton(button3.gameObject);

        if (Text3.text == array[NumQest].repJuste ){
            buttonPressedIf();
        }else{
            buttonPressedElse();
        }
       
        //Debug.Log("retour"+ retour);

        button3Clicked = false;
        buttonPressed();
    }

    public void EnableButton(GameObject button)
    {
        button.SetActive(true);
        Background.SetActive(true);
    }

    // Function to disable a button
    public void DisableButton(GameObject button)
    {
        button.SetActive(false);
        Background.SetActive(false);
    }
    
    void Shuffle (int[] deck) {
        for (int i = 0; i < deck.Length; i++) {
            int temp = deck[i];
            int randomIndex = Random.Range(i, deck.Length);
            deck[i] = deck[randomIndex];
            deck[randomIndex] = temp;
        }
    }
    private void initialise(){
        //Lvl 1
        array[0]= new QA("Quel est le pays souvent associé aux pâtes ?", "L'Italie", "Le Portugal", "Le Japon", "L'Italie");
        array[1]= new QA("Quel est le continent où se trouve l'Égypte?", "L'Afrique", "L'Asie", "L'Amérique du Sud", "L'Afrique");
        array[2]= new QA("Quelle est la capitale de la France?", "Paris", "Paris", "Marseille", "Lyon");
        array[3]= new QA("Où se trouve la Grande Muraille de Chine?", "En Chine", "Au Japon", "En Chine", "Au Cambodge");
        array[4]= new QA("Quel pays n'est pas célèbre pour ses pyramides?", "Les États-Unis", "Le Mexique", "L'Égypte", "Les États-Unis");
        array[5]= new QA("Où se trouve la Tour Eiffel?", "À Paris", "À Bordeaux", "À Paris", "À Rome");
        array[6]= new QA("Quel est le pays le plus peuplé du monde?", "L'Inde", "La Chine", "La Russie", "L'Inde");
        array[7]= new QA("Quelle est la capitale du Japon?", "Tokyo", "Osaka", "Kyoto", "Tokyo");
        array[8]= new QA("Quelle est la capitale de la Suisse", "Berne", "Genève", "Zürich", "Berne");
        array[9]= new QA("Quelle est la capitale de l'Espagne?", "Madrid", "Madrid", "Barcelone", "Málaga");
        array[10]= new QA("Quel est le plus grand désert froid du monde?", "L’Antarctique ", "Le pôle Nord", "Le pôle Sud", "L’Antarctique");
        array[11]= new QA("Quel est le plus grand pays d'Amérique du Sud?", "Le Brésil ", "Le Brésil", "Le Pérou", "L’Argentine");
        array[12]= new QA("Dans quel continent se trouve l'Inde?", "En Asie", "En Afrique", "En Asie", "En Indochine");
        array[13]= new QA("Quel est le plus grand désert chaud du monde?", "Le Sahara", "Le Sahara", "Le désert Arctique", "Désert d'Arabie");
        array[14]= new QA("Quel est le plus grand pays du monde par sa superficie?", "La Russie", "Les États-Unis", "Le Brésil", "La Russie");
        array[15]= new QA("Où se trouve la ville de Sydney?", "En Australie", "Aux États-Unis", "En Australie", "En Angleterre");
        array[16]= new QA("Où se trouvent les chutes du Niagara?", "Au Canada", "Au Nicaragua", "Au Canada", "Au Nigeria");
        array[17]= new QA("Où se trouve la Statue de la Liberté?", "À New-York, aux États-Unis", "À New-York, aux États-Unis", "À Las Vegas, aux États-Unis", "À Washington DC, aux États-Unis");
        array[18]= new QA("Quel est le plus grand canyon du monde??", "Le Grand Canyon (États-Unis)", "Le Géant Canyon (États-Unis)", "Le Big Canyon (États-Unis)", "Le Grand Canyon (États-Unis)");
        array[19]= new QA("Dans quel continent se trouve la Russie?", "En Europe et en Asie", "En Europe", "En Europe et en Asie", "En Asie");
        //Lvl 2
        array[20]= new QA("Quelle est la capitale de la Turquie?", "Ankara", "Istanbul", "Turkménistan", "Ankara");
        array[21]= new QA("Où se trouve la ville de Pékin?", "En Chine", "Au Tibet", "En Chine", "En Côte d'Ivoire");
        array[22]= new QA("Quel est le plus grand océan du monde?", "L'océan Pacifique", "L'océan Indien", "L'océan Pacifique", "L'océan Atlantique");
        array[23]= new QA("Où se trouve le mont Fuji?", "Au Japon", "En Corée du Sud", "Au Japon", "En Chine");
        array[24]= new QA("Où se trouve la ville de Marrakech?", "Au Maroc", "Au Maroc", "En Algérie", "Au Mexique");
        array[25]= new QA("Quel est le plus petit continent du monde?", "L'Océanie", "L'Australie", "L'Europe", "L'Océanie");
        array[26]= new QA("Quel est le pays le plus peuplé d'Europe?", "La Russie", "La France", "La Russie", "L'Allemagne");
        array[27]= new QA("Dans quel continent se trouve le Pérou?", "En Amérique du Sud", "En Amérique du Sud", "En Amérique Centrale", "En Afrique");
        array[28]= new QA("Quel pays est réputé pour ses danses flamenco?", "L'Espagne", "Le Méxique", "L'Espagne", "Le Brésil");
        array[29]= new QA("Où peut-on visiter le Colisée?", "En Italie", "En Italie", "En Grèce", "En Roumanie");
        array[30]= new QA("Quelle nation est connue pour son mont Olympe?", "La Grèce", "La Grèce", "Lausanne", "La Russie");
        array[31]= new QA("Quelle est la capitale du Canada?", "Ottawa", "Toronto", "Montréal", "Ottawa");
        array[32]= new QA("Quelle est la capitale de l'Argentine?", "Buenos Aires", "Buenos Aires", "Córdoba", "Mendoza");
        array[33]= new QA("Quel volcan a enseveli la ville de Pompéi?", "Le Vésuve", "Le Stromboli ", "Le Vésuve", "L’Etna");
        array[34]= new QA("Quel est le plus haut sommet du monde?", "L’Everest", "L’Himalaya", "Le Mont-Blanc", "L’Everest");
        array[35]= new QA("Quel est plus grand fleuve du monde?", "Le Nil", "Le Nil", "L’Amazone", "Le Danube");
        array[36]= new QA("Quelle est la capitale de l'Égypte?", "Le Caire", "Luxor", "Le Caire", "Alexandrie");
        array[37]= new QA("Quelle est la capitale de la Pologne?", "Varsovie", "Cracovie", "Lublin", "Varsovie");
        array[38]= new QA("Quelle est la capitale de la Russie?", "Moscou", "Saint-Pétersbourg", "La Sibérie", "Moscou");
        array[39]= new QA("Quelle est la capitale de la Thaïlande?", "Bangkok", "Pattaya", "Bangkok", "Phuket");
        //Lvl 3
        array[40]= new QA("Quelle est la capitale de l'Afrique du Sud?", "Pretoria", "Johannesburg", "Pretoria", "Port Elizabeth");
        array[41]= new QA("D'où sont originaires les pâtes?", "De Chine", "Du Japon", "De Chine", "D'Italie");
        array[42]= new QA("Quelle est la capitale de la Norvège?", "Oslo", "Oslo", "Stockholm", "Malmo");
        array[43]= new QA("Où se trouve la ville de Vladivostok?", "En Russie", "En Pologne", "En Croatie", "En Russie");
        array[44]= new QA("Quelle est la capitale du Kenya?", "Nairobi", "Mombasa", "Nairobi", "Dakar");
        array[45]= new QA("Où se trouve la cordillère des Andes?", "En Amérique du Sud", "En Amérique du Sud", "En Europe", "En Amérique du Nord");
        array[46]= new QA("Quelle est la capitale de l'Irlande?", "Dublin", "Édimbourg", "Dublin", "Belfast");
        array[47]= new QA("Quel est le plus grand pays d'Afrique", "L'Algérie", "L'Algérie", "L'Afrique du Sud", "La République Démocratique du Congo");
        array[48]= new QA("Où se trouve le canal de Panama?", "Au Panama", "En Ipanema", "Au Panama", "Au Chili");
        array[49]= new QA("Quelle est la capitale de l'Inde?", "New Delhi", "Calcutta", "New Delhi", "Mumbai");
        array[50]= new QA("Quel est le plus grand lac salé du monde?", "La mer Caspienne", "La mer Rouge", "Le lac du Michigan", "La mer Caspienne");
        array[51]= new QA("Quelle est la plus grande ville d'Inde?", "Mumbai (Bombay)", "Jaipur", "Mumbai (Bombay)", "Delhi");
        array[52]= new QA("Quel est le plus grand archipel européen?", "La Finlande", "La Finlande", "Les Îles Canaries", "Malte");
        array[53]= new QA("Quelle est la plus grande île de Méditerranée?", "La Sicile", "Capri", "Ibiza", "La Sicile");
        array[54]= new QA("Où se trouve le désert du Gobi?", "En Asie", "En Afrique", "En Asie", "En Amérique du Sud");
        array[55]= new QA("Quelle est la plus grande ville d'Afrique?", "Le Caire", "Marrakech", "Le Caire", "Alger");
        array[56]= new QA("Quel est le plus petit pays du monde en superficie?", "Le Vatican", "Le Vatican", "Monaco", "Le Liechtenstein");
        array[57]= new QA("Quel est le pays le plus densément peuplé du monde?", "Monaco", "La Chine", "Monaco", "L'Inde");
        array[58]= new QA("Où se trouve le détroit de Gibraltar?", "Entre l'Espagne et le Maroc", "Entre l'Espagne et le Portugal", "Entre l'Espagne et le Maroc", "Entre le Portugal et le Maroc");
        array[59]= new QA("Quel est le nom de la plus grande forêt tropicale du monde?", "L'Amazonie", "La forêt tropicale humide du Congo", "L'Amazonie", "La forêt tropicale birmane");
        //Lvl 4
        array[60]= new QA("Où se trouve le mont Kilimandjaro?", "En Tanzanie", "En Tanzanie", "Au Kenya", "À Madagascar");
        array[61]= new QA("Quelle est la capitale de l’Australie?", "Canberra", "Melbourne", "Sydney", "Canberra");
        array[62]= new QA("Quel est le plus grand archipel d’Asie?", "L’Indonésie", "Les Philippines", "L’Indonésie", "Le Japon");
        array[63]= new QA("Quelle est la plus haute montagne d'Europe?", "Le mont Elbrouz", "Le mont Olympe", "Le mont Elbrouz", "Le mont Blanc");
        array[64]= new QA("Où se trouve le désert de Sonora?", "Aux États-Unis et au Mexique", "Au Mexique", "Aux États-Unis et au Mexique", "Aux États-Unis");
        
        array[65]= new QA("Quel est le plus long fleuve d'Europe?","Le Volga", "Le Rhône", "Le Danube", "Le Volga");
        array[66]= new QA("Où se trouve le désert de Kalahari?", "En Afrique", "En Afrique", "En Australie", "En Inde");
        array[67]= new QA("Où se trouve le parc national de Yellowstone?", "Aux États-Unis", "Au Canada", "Aux États-Unis", "En Angleterre");
        array[68]= new QA("Dans quel pays trouve-t-on le mont Elbrouz?", "En Russie", "Au Monténégro", "En Russie", "En Algérie");
        array[69]= new QA("Quelle rivière traverse la ville de Londres?", "La Tamise", "La Meuse", "La Tamise", "La Seine");
        array[70]= new QA("Quelle est la plus grande île au monde?", "Le Groenland", "Haïti", "Le Groenland", "L'Australie");
        array[71]= new QA("Quel est le plus grand fleuve d'Amérique du Sud?", "L'Amazone", "Le Rio Paraná", "Rio de Janeiro", "L'Amazone");
        array[72]= new QA("Quel est le pays avec le plus grand nombre de volcans actifs?", "L’Indonésie", "L’Indonésie", "Hawaii", "L’Italie");
        array[73]= new QA("Où se trouve le plus grand port naturel du monde?", "À Rio de Janeiro", "À Rio de Janeiro", "À Alexandrie", "À Sydney");
        array[74]= new QA("Quelle est la capitale de la Nouvelle-Zélande?", "Wellington", "Christchurch", "Wellington", "Nelson");
        array[75]= new QA("Quel est le plus grand delta du monde?", "Le delta du Gange", "Le delta du Rhône et de l'Arve", "Le delta du Nil", "Le delta du Gange");
        array[76]= new QA("Où se trouve la ville de Calgary?", "Au Canada", "Au Canada", "En Irlande", "En Angleterre");
        array[77]= new QA("Où se trouve la ville de Ljubljana?", "En Slovénie", "En Serbie", "Au Kosovo", "En Slovénie");
        array[78]= new QA("D'où vient le Tigre du Bengale?", "D'Inde", "D'Inde", "Du Bengladesh", "Du Cameroun");
        array[79]= new QA("Parmi les parcs nationaux américains suivants, lequel n'existe pas?", "Parc national de Big Tree", "Parc national de Yosemite", "Parc national des Sequoias", "Parc national de Big Tree");
        //Lvl 5
        array[80]= new QA("Quel est le point le plus bas de la Terre?", "La mer Morte", "La mer Morte", "L’Enfer", "Les Bouches-du-Rhône");
        array[81]= new QA("Quel est le plus grand port de commerce du monde?", "À Shangai, en Chine", "À Shangai, en Chine", "À Anvers, en Belgique", "À Hambourg, en Allemagne");
        array[82]= new QA("Où se trouve le co   l du Stelvio", "En Italie", "En Espagne", "En Italie", "En France");
        array[83]= new QA("Où se trouve le désert d'Atacama?", "Au Chili", "Au Chili", "Au Mexique", "Au Pérou");
        array[84]= new QA("Où se trouve le lac Titicaca?", "Entre le Pérou et la Bolivie", "Au Mexique", "Entre le Pérou et la Bolivie", "Aux toilettes");
        array[85]= new QA("Quel est le plus haut sommet d'Amérique du Nord?", "Le mont Denali", "Le mont Whitney", "Le mont Denali", "Le mont Logan");
        array[86]= new QA("Quelle est la plus grande île d'Asie?", "Bornéo", "Le Japon", "Les Philipinnes", "Bornéo");
        array[87]= new QA("Dans quelle région de la Russie trouve-t-on la république de Sakha?", "En Sibérie orientale", "À l'Amour", "En Sibérie orientale", "À Belgorod");
        array[88]= new QA("Quel est le plus grand canyon du monde?", "Le canyon de Fish River", "Le Kings Canyon", "Le Grand Canynon", "Le canyon de Fish River");
        array[89]= new QA("Dans quel pays se trouve la ville de Cusco?", "Au Pérou", "Au Chili", "Au Pérou", "Au Mexique");
        array[90]= new QA("Quel est le point le plus chaud de la planète?", "Le Désert de Lut (Iran)", "Dubaï", "Le Désert de Lut (Iran)", "La Vallée de la Mort (USA)");
        array[91]= new QA("Où se trouve le mont Cook?", "En Nouvelle-Zélande", "En Nouvelle-Zélande", "En cuisine", "En Irlande");
        array[92]= new QA("Quel est le plus grand lac d’eau douce du monde?", "Le Lac Supérieur", "Le lac Léman", "La mer Caspienne", "Le Lac Supérieur");
        array[93]= new QA("Quel est le plus haut volcan actif du monde?", "Le Mauna Loa (Hawaï)", "L’Etna", "Le Mauna Loa (Hawaï)", "Le Cotopaxi");
        array[94]= new QA("Quelle est la plus haute cascade du monde?", "Le Salto Angel (Venezuela)", "Le Salto Angel (Venezuela)", "Les chutes du Niagara(Canada)", "Les Chutes du Reichenbad (Suisse)");
        array[95]= new QA("Quelle est la plus haute montagne en Amérique du Sud?", "L'Aconcagua (Argentine)", "La Cordillère des Andes ", "Monte Pissis(Argentine)", "L'Aconcagua (Argentine)");
        array[96]= new QA("Quelle est la plus haute capitale du monde?", "La Paz (Bolivie)", "Quito (Équateur)", "Bogota (Colombie)", "La Paz (Bolivie)");
        array[97]= new QA("Quelle est la plus haute cascade d'Afrique?", "Le Tugela Falls", "Le Lisbon Falls", "Le Tugela Falls", "Les chutes Victoria");
        array[98]= new QA("Où se trouve le volcan Krakatoa?", "En Indonésie", "En Indonésie", "À Hawaii", "Aux Philippines");
        array[99]= new QA("Dans quel pays trouve-t-on le plateau tibétain?", "En Chine", "Au Tibet", "Au Népal", "En Chine");

    }

    private void initialiseAll(){
        NumNiveau=0;
        Question.text = " ";
        Delta =17;
        //posRetour = 0;
        //Shuffle(ArrayNombre);
    }

    IEnumerator CheckGameOverCoroutine(){
        Debug.Log("in");
        yield return new WaitForSeconds(1);
        Question.text= " ";
    }

    private void buttonPressed(){
        Delta += 20;
        QuestionPosée++;
        Ratio = (double)Juste / (double)QuestionPosée;
        NumQestCalcule=0;
        Debug.Log("Juste "+ Juste +", Question "+ QuestionPosée + ", Ratio "+Ratio);
        TextAvancé.text= "Vous êtes à la question "+ QuestionPosée+" \nVous avez "+Juste+"/20 réponses justes";

    }

    private void buttonPressedIf(){
        NumNiveau++;
        posRetour = NumNiveau*20+2;
        Balle.transform.position = new Vector3(0, 1, posRetour);   //51, 1, 0
        Juste++;
        Question.text= "Juste";
        StartCoroutine(CheckGameOverCoroutine());
    }

    private void buttonPressedElse(){
        NumNiveau++;
        posRetour = NumNiveau*20+2;
        Balle.transform.position = new Vector3(0, 1, posRetour);   //51, 1, 0
        Erreur= Erreur+ Question.text +": "+ array[NumQest].repJuste +"\n";
        Question.text= "Faux";
        StartCoroutine(CheckGameOverCoroutine());
    }



    private void CalculeNum(){
        if (Ratio < 0.2){
            NumQest = Question1%20;
            NumQest = ArrayNombre1[NumQest];
            Question1++;
            Debug.Log("NumQest1 "+NumQest);
        }else if (Ratio >= 0.2 && Ratio < 0.4){
            NumQest = Question2%20;
            NumQest = ArrayNombre2[NumQest];
            Question2++;
            Debug.Log("NumQest2 "+NumQest);
        }else if (Ratio >= 0.4 && Ratio < 0.6){
            NumQest = Question3%20;
            NumQest = ArrayNombre3[NumQest];
            Question3++;
            Debug.Log("NumQest3 "+NumQest);
        }else if (Ratio >= 0.6 && Ratio < 0.8){
            NumQest = Question4%20;
            NumQest = ArrayNombre4[NumQest];
            Question4++;
            Debug.Log("NumQest4 "+NumQest);
        }else if (Ratio >= 0.8){
            NumQest = Question5%20;
            NumQest = ArrayNombre5[NumQest];
            Question5++;
            Debug.Log("NumQest5 "+NumQest);
        }
    }
}
