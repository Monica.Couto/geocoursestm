

[System.Serializable]

public class QA
{
    public string questionItem;
    public string repJuste;
    public string itemName1;
    public string itemName2;
    public string itemName3;

    public QA(string qest, string rep, string name1, string name2, string name3)
    {
        questionItem = qest;
        repJuste = rep;
        itemName1 = name1;
        itemName2 = name2;
        itemName3 = name3;
    }
}
